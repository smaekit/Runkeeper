//
//  ViewController.swift
//  Runkeeper
//
//  Created by Marcus Jakobsson on 2017-11-06.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    var locationManager: CLLocationManager!
    var latestLocation : CLLocation?
    var runTimer = Timer()
    var totalRunInSeconds = 0
    var totalRunInMeters  = 0
    var thousandMeters = 1000
    var audioPlayer = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.mapType = MKMapType(rawValue: 0)!
        mapView.userTrackingMode = MKUserTrackingMode(rawValue: 2)!
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.delegate = self;
        
        //Checks if the user have granted permisson to use Gps-location when app is running in foreground else asks the user for it.
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        {
            
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()

        } else
        {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
        
    }
    
    func playApplaudeSound()
    {
        do
        {
            audioPlayer = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "applause10", ofType: "wav")!))
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        }
        catch
        {
            print("Could not play audio")
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let state = UIApplication.shared.applicationState
        if state == .active
        {
            
            if !(runTimer.isValid)
            {
                startTimer()
            }
            
            let currentLocation = locations[0]
            
            if let distance = latestLocation?.distance(from: currentLocation)
            {
                if !(distance > 15)
                {
                    totalRunInMeters += Int(distance)
                    distanceLabel.text = "Distance(Meters): " + String(totalRunInMeters)
                }
            }
            
            var area = [currentLocation.coordinate, currentLocation.coordinate]
            let route = MKPolyline(coordinates: &area, count: area.count)
            
            //set current location to previous location when done
            latestLocation = currentLocation
            mapView.add(route)
            
            
            if totalRunInMeters > thousandMeters
            {
                playApplaudeSound()
                thousandMeters += 1000
            }
        }
        else
        {
            runTimer.invalidate()
        }
    }
    
    private func startTimer()
    {
        runTimer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(ViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    //Called every second
    @objc func updateTimer(time: TimeInterval) {
        totalRunInSeconds += 1
        
        let hours = Int(totalRunInSeconds) / 3600
        let minutes = Int(totalRunInSeconds) / 60
        let seconds = totalRunInSeconds % 60
        
        timeLabel.text = "Time: " + String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
}
extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        
        guard let polyline = overlay as? MKPolyline else {
            return MKOverlayRenderer(overlay: overlay)
        }
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.strokeColor = .red
        renderer.lineWidth = 3
        return renderer
    }
    
}
